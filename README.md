# todo

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Features
## Mandotary 
Adding tasks to database, X
Displaying tasks from database, X 
Checking off tasks,  

## Desired
Adding tasks weeks in advance, 
Ability to see completed tasks, 
Ability to change or update tasks that have already been set
mobile version 
create navbar with buttons i.e easy access to buttons on mobile
filtering- important, day, search


## Luxury
Adding tasks months in advance, 
Changing the priority of tasks, 
Two lists, One list for all tasks then a second for the current day, 
flag certain tasks as very important i.e full list of tasks, tasks for the current day, list of important tasks

### Standard
Camelcase variable names 
i.e dayToBeCompleted 

## What is a task
the actual task: string
id: autonumber
day it is due: date
whether or not it is completed: boolean
timestamps for creation and last update


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
