import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.css'
import VueEvents from 'vue-events'
import VueWait from 'vue-wait'

Vue.use(BootstrapVue)
Vue.use(VueEvents)
Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.use(require('vue-moment'))
Vue.use(VueWait)

new Vue({
  router,
  store,
  render: h => h(App),
  wait: new VueWait({
    useVuex: false,
    vuexModuleName: 'wait',
    registerComponent: true,
    componentName:"v-wait",
    registerDirective: true,
    directiveName: 'wait'
  })
}).$mount('#app')
