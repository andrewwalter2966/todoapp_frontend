describe('Test functionality within the homepage', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080')
    })

    it('displays any information contained in the database', () => {
        cy.get('#filter-dropdown').click().get('#btnNoFil').click()
        cy.get('#task-container').contains('1')
    })

    it('has an Add button that adds tasks', () => {
        cy.get('#btn-add').click()
        cy.wait(500)
        cy.get('#addInputTask').type('Test the functionality - A23o4Iu21')
        cy.wait(500)
        cy.get('#addInputDay').click().type('Invalid date') //Unable to input a valid date via standard means, testing continues using invalid date
        cy.contains('OK').click()
        cy.get('#filter-dropdown').click().get('#btnNoFil').click()
        cy.get('#task-container').contains('Test the functionality - A23o4Iu21')
    })

    it('has a button that updates the information of an object', () => {
        cy.get('#filter-dropdown').click().get('#btnNoFil').click()
        cy.get('#task-container').contains('Test the functionality - A23o4Iu21').click()
        cy.get('#btn-upd').click()
        cy.wait(500)
        cy.get('#updateInputTask').type('Test the functionality - A23o4Iu22')
        cy.wait(500)
        cy.get('#updateInputDay').type('Invalid date') //Unable to input a valid date via standard means, testing continues using invalid date
        cy.contains('Confirm').click()
        cy.get('#task-container').contains('Test the functionality - A23o4Iu22')
        
    })

    it('has a button that deletes records from the api', () => {
        cy.get('#filter-dropdown').click().get('#btnNoFil').click()
        cy.get('#task-container').contains('Test the functionality - A23o4Iu22').click()
        cy.get('#btn-del').click()
    })

    //Following two test methods to be completed when buttons work properly
    it('has a priority checkbox that modifies its behaviour', () => {
        cy.get('#btn-add').click()
        cy.wait(500)
        cy.get('#addInputTask').type('Test the functionality - A23o4Iu25')
        cy.wait(500)
        cy.get('#addInputDay').click().type('Invalid date') //Unable to input a valid date via standard means, testing continues using invalid date
        cy.wait(500)
        cy.get('#addInputTask').click()
        cy.contains('Priority Task?').click()
        cy.contains('OK').click()
        cy.get('#filter-dropdown').click().get('#btnPriority').click()
        cy.get('#task-container').contains('Test the functionality - A23o4Iu25')
        //Testing for background color canceled due to difficulties properly locating independent items
        //.should('have.css', 'background-color', 'rgb(255, 195, 0)')
    })

    it('has a button that sets the task as "completed" and modifies its behaviour', () => {
        cy.get('#filter-dropdown').click().get('#btnNoFil').click()
        cy.get('#task-container').contains('Test the functionality - A23o4Iu25').contains('Task completed?').click()
        cy.get('#filter-dropdown').click().contains('Completed Tasks').click()
        cy.get('#task-container').contains('Test the functionality - A23o4Iu25')
        //Testing for background color canceled due to difficulties properly locating independent items
        //.should('have.css', 'background-color', 'rgb(14, 206, 232)')

        //Deleting the record to keep the server clean
        cy.get('#task-container').contains('Test the functionality - A23o4Iu25').click()
        cy.get('#btn-del').click()
    })
})