describe('Test HTML within the homepage', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080')
    })

    it('has a container with a header named "Tasks"', () => {
        cy.get('#task-container').contains('Tasks')
    })

    it('contains a drop-down list that contains several buttons', () => {
        cy.get('#filter-dropdown').click().get('#btnDate').get('#btnNoFil').get('#btnComplete').get('#btnPriority')
    })

    it('contains buttons for CRUD functionality', () => {
        cy.get('#btn-add').contains('Add Task')
        cy.get('#btn-del').contains('Delete Task')
        cy.get('#btn-upd').contains('Update')
    })

    // it('contains buttons that extend CRUD functionality', () => {
    //     cy.get('#btn-multidelete').contains('Delete Multiple Tasks')
    // })

    it('contains a string that displays the version number', () => {
        cy.contains('Version ')
    })
})