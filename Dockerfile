FROM node:latest
RUN ["npm","install","-g","http-server"]
RUN ["npm","install","vue-moment"]
WORKDIR /app
COPY package*.json ./
RUN ["npm","install"]
COPY . .
RUN ["npm","run","build"]
EXPOSE 8080
CMD ["http-server","dist"]